from os_commands import *
import sys


backup_to_restore = sys.argv[1]
backup_directory = '/'.join(backup_to_restore.split('/')[:-1])
destination = get_backuped_path(backup_directory)

print(f'Welcome to Backupy - Restore !')
print(f'About to restore :')
print(f'{backup_to_restore}')
print(f'into')
print(f'{destination}')
answer = input('Are you sure you want to continue ? (y/N)')

if answer.lower() == 'y':
    empty_directory(directory=destination)
    untar_directory(archive_tar=backup_to_restore, destination=destination)
    print('Backup restored.')
else:
    print('Backup cancelled.')


