import os


def tar_directory(directory_to_tar, destination_tar):
    leading_path = '/'.join(directory_to_tar.split('/')[:-1])
    directory = directory_to_tar.split('/')[-1]
    os.system(f'tar -czvf {destination_tar} -C {leading_path} {directory} > /dev/null')


def untar_directory(archive_tar, destination):
    os.system(f'tar -xf {archive_tar} -C {destination} --strip-components=1 > /dev/null')


def prune_directory(directory_to_prune, retention):
    os.system(f"find {directory_to_prune} -name '*.tar.gz' -mtime +{retention} -delete > /dev/null")


def empty_directory(directory):
    os.system(f'rm -rf {directory}/* > /dev/null')


def create_directory_path_if_not_exist(directory):
    os.system(f'mkdir -p {directory} > /dev/null')


def write_info_file(backup_dir, backuped_dir):
    os.system(f'echo "{backuped_dir}" > {backup_dir}/.info')


def get_backuped_path(backup_dir):
    path = ''
    with open(f'{backup_dir}/.info', 'r') as f:
        path = f.readline().replace('\n', '')
    return path
