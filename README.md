# Backupy

Simple local backuping solution with Python, meant to be crontabed.

## Disclaimer

This is just a training project and it's not even close to be ready to be used in production. Use at your own risks !

## How to use : Backuping

1. Clone sources. They will be refered as `SRC/` directory for now on.
2. Edit your crontab with `crontab -e`. Add one line for each directory you want to backup.

```bash
0 0 * * * python SRC/backup.py WHERE_TO_STORE_BACKUPS DIRECTORY_1_TO_BACKUP 30
0 1 * * * python SRC/backup.py WHERE_TO_STORE_BACKUPS DIRECTORY_2_TO_BACKUP 30
0 2 * * * python SRC/backup.py WHERE_TO_STORE_BACKUPS DIRECTORY_3_TO_BACKUP 30
```

In this example, the first directory will be backuped at midnight, the second one at 1AM, the third one at 2AM.  
The last argument (30) is the retention policy you want to have in days. In this case, every backup older than 30 days
will be deleted during the backuping process.


> There's no need to use a different storing directory for each backups. In this example, Backupy will create the 
following hierarchy by itself:

```
WHERE_TO_STORE_BACKUPS/
    DIRECTORY_1_TO_BACKUP/
        DIRECTORY_1_backup.tar.gz
    DIRECTORY_2_TO_BACKUP/
        DIRECTORY_2_backup.tar.gz
    DIRECTORY_3_TO_BACKUP/
        DIRECTORY_3_backup.tar.gz
```

## How to use : Restore

Simply use this command :

```bash
python SRC/restore.py BACKUP.tar.gz
```

With the previous example, it could be something like :

```bash
python SRC/restore.py WHERE_TO_STORE_BACKUPS/DIRECTORY_1_TO_BACKUP/DIRECTORY_1_backup.tar.gz
```
