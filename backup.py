from os_commands import *
from datetime import date
import sys

if len(sys.argv) < 4:
    print('Wrong number of arguments, exiting before doing anything risky.')
    exit(-1)

general_backups_dir = sys.argv[1]
directory_to_backup = sys.argv[2]
retention = sys.argv[3]

timestamp = date.today().strftime("%Y%m%d")
directory_name = directory_to_backup.split('/')[-1]
backups_dir = f'{general_backups_dir}/{directory_name}'
target_archive = f'{backups_dir}/{directory_name}_{timestamp}.tar.gz'


create_directory_path_if_not_exist(directory=backups_dir)
write_info_file(backup_dir=backups_dir, backuped_dir=directory_to_backup)
tar_directory(directory_to_tar=directory_to_backup, destination_tar=target_archive)
prune_directory(directory_to_prune=backups_dir, retention=retention)
